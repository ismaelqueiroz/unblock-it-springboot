package com.everis.unblockit.metrics

import io.micrometer.core.instrument.MeterRegistry
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component

@Component
class MongoMeterIndicator @Autowired constructor(private val meterRegistry: MeterRegistry) {

    @Bean
    fun mongoStatusProbe(template: MongoTemplate): MongoStatusProbe? {
        return MongoStatusProbe(template)
    }

}