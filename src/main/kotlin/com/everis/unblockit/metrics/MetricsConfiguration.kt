package com.everis.unblockit.metrics

import com.mongodb.MongoClientSettings
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.binder.mongodb.MongoMetricsCommandListener
import io.micrometer.core.instrument.binder.mongodb.MongoMetricsConnectionPoolListener
import org.springframework.boot.autoconfigure.mongo.MongoClientSettingsBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


/**
 * Class to configure binders for micrometer metrics for this application
 */
@Configuration
class MetricsConfiguration {
    @Bean
    fun mongoClientSettingsBuilderCustomizer(meterRegistry: MeterRegistry?): MongoClientSettingsBuilderCustomizer? {
        return MongoClientSettingsBuilderCustomizer { builder: MongoClientSettings.Builder ->
            run {
                builder.addCommandListener(MongoMetricsCommandListener(meterRegistry!!))
                        .applyToConnectionPoolSettings {
                            it.addConnectionPoolListener(MongoMetricsConnectionPoolListener(meterRegistry))
                        }
            }
        }
    }
}