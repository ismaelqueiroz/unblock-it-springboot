package com.everis.unblockit.repository

import com.everis.unblockit.model.WeatherForecast
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WeatherForecastRepository : CrudRepository<WeatherForecast, String> {
    fun findAll(pageable: Pageable): Page<WeatherForecast>
}