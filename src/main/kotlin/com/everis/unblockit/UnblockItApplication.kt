package com.everis.unblockit

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class UnblockItApplication

fun main(args: Array<String>) {
	runApplication<UnblockItApplication>(*args)
}


