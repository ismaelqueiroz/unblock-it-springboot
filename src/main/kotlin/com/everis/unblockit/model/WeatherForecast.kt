package com.everis.unblockit.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document("weatherforecast")
data class WeatherForecast(@Id val id: String, val date: String, val temperatureC: Int,
                           val temperatureF: Int, val summary: String)