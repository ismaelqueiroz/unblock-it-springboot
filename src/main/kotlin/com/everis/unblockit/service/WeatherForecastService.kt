package com.everis.unblockit.service

import com.everis.unblockit.repository.WeatherForecastRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class WeatherForecastService @Autowired constructor(private val weatherForecastRepository: WeatherForecastRepository) {
    fun getAll(pageable: Pageable) = weatherForecastRepository.findAll(pageable)
    fun get(id: String) = weatherForecastRepository.findById(id);
}