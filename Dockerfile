FROM adoptopenjdk:14-hotspot

LABEL maintainer="Ismael Queiroz <eu@queiroz.dev>" \
  org.label-schema.name="Unblock IT (springboot)" \
  org.label-schema.vendor="Observability & Monitoring" \
  org.label-schema.schema-version="1.0.0-RC"

# default to timeZone
ENV TZ America/Sao_Paulo

# Turn down the verbosity to default level.
ENV NPM_CONFIG_LOGLEVEL warn

# default to UTF-8 file.encoding
ENV LANG C.UTF-8

# set /app directory as default working directory
# Adding unblockit user and group
RUN useradd --create-home \
    --user-group \
    --shell /bin/bash \
    --home-dir /home/unblockit \
    unblockit

## set /app directory as default working directory
WORKDIR /home/unblockit

## copy all file from current dir to /app in container
COPY --chown=unblockit ./build/libs/unblock-it-*.jar ./app.jar

# set user
USER unblockit

## expose port 8080
EXPOSE 8081

## cmd to start service
CMD [ "java", "-Duser.country=BR", "-Duser.language=pt", "-jar", "app.jar" ]
